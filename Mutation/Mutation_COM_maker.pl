#!/usr/bin/env perl
#!/usr/bin/perl -s

#Version notes:
# com files correctly
# numeration of files done correctly
# commented



#perl script.pl MODE ARGS CONFIG INPUT NAME_OF_COM
my $file=$ARGV[3]; #XYZ file name
my $name=$ARGV[4]; #NAMe of the .com files
my $type=$ARGV[0];	#Option to work with. Q for quantity, e for energy cut.
my $configFile= $ARGV[2];	#Config file with all the data for .com file

#Config file data is turn in variables.
my $NProc;
my $Mem;
my @command=();
my $ChargeMulti;
my $charge, $multi;
######################################
# Var from the script
my $other_element = 0.8;
my $totalQuantity;
my $actualComInWork = 0;
my $currentJ, $bestJ, $worstJ;
my $natom;
my $delta           = 0.1;
my $FIXEDPCENTUMBRAL = 0.667;
my $PCENTATOMMUTATED = 0.3;
################
my %Atomic_radii = ( 'H'  => '0.4', 'He' => '0.3', 'Li' => '1.3', 'Be' => '0.9', 
                     'B'  => '0.8', 'C'  => '0.8', 'N'  => '0.8', 'O'  => '0.7', 
					 'F'  => '0.7', 'Ne' => '0.7', 'Na' => '1.5', 'Mg' => '1.3', 
					 'Al' => '1.2', 'Si' => '1.1', 'P'  => '1.1', 'S'  => '1.0', 
					 'Cl' => '1.0', 'Ar' => '1.0', 'K'  => '2.0', 'Ca' => '1.7', 
					 'Sc' => '1.4', 'Ti' => '1.4', 'V'  => '1.3', 'Cr' => '1.3', 
					 'Mn' => '1.4', 'Fe' => '1.3', 'Co' => '1.3', 'Ni' => '1.2', 
					 'Cu' => '1.4', 'Zn' => '1.3', 'Ga' => '1.3', 'Ge' => '1.2', 
					 'As' => '1.2', 'Se' => '1.2', 'Br' => '1.1', 'Kr' => '1.1', 
					 'Rb' => '2.1', 'Sr' => '1.9', 'Y'  => '1.6', 'Zr' => '1.5', 
					 'Nb' => '1.4', 'Au' => '1.4' );


sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };
# This functions returns the covalent radii from a specific atom, given the atom name
sub value_covalent_radii {
	my ($element) = @_;
	my $radii_val = 0;
	if ( exists $Atomic_radii{$element} ) {
		$radii_val = $Atomic_radii{$element};
	} else {
		$radii_val = $other_element ;
	}	
	return $radii_val
}
#this function takes a array and shuffle its elements
sub fisher_yates_shuffle {
	my $array = shift;
	my $i = @$array;
	while ( --$i ) {
		my $j = int rand( $i+1 );
		@$array[$i,$j] = @$array[$j,$i];
	}
	return @$array; 
}
#This function calculates a probability for an specific cluster to mutate.
sub CalculateProbToMutatePosition{
	$actualComInWork++;
	#print "\t\t$actualComInWork de $totalQuantity\n";
	my $quadTotal=$totalQuantity*$totalQuantity;
	my $quadActual=$actualComInWork*$actualComInWork;
	my $actualNormalized=( $quadActual)/$quadTotal;
	#print "\t\t$actualNormalized \n";
	return $actualNormalized;
}
sub CalculateProbToMutateWithJ{
	print "J actual original: $currentJ";
	my $restaToNormalize=($bestJ-$worstJ);
	#my $quadBest=($bestJ-$worstJ)**2;
	my $restJ=$currentJ-$worstJ;
	my $currentNormalized=$restJ/$restaToNormalize;
	print "Current = $currentNormalized\n";
	my $FirstIsBadMutant=1-$currentNormalized;
	print "Current Fixed = $FirstIsBadMutant\n\n";
	return $FirstIsBadMutant;
}
sub PrepareMutantBackground{
	my ($best, $worst)=@_;
	my $resta=$best-$worst;

}
#################################################
# Verification
sub verification{
	my ($a1, $a2, $dist)=@_;
	# hash values	
	my $v1=$Atomic_radii{$a1} || $Atomic_radii{default}; 
	my $v2=$Atomic_radii{$a2} || $Atomic_radii{default};
	my $sum= $v1+$v2;  
	my $resultado;
	# steric effects if radio1+radio2 < distance
	#print "Distancia vs real = $dist  ?  $sum\n";
	if($dist <= $sum){
		# Steric problem	
		$resultado = 1; 
	}else{
		$resultado = 0;
	}
	return $resultado;
}
###################################
# Distance between one point to all other in a coord xyz
sub distance_point {
	# array coords basin 1 and basin 2
	my ($p1,$p2,$p3, $x1, $y1, $z1) = @_;
	# measure distance between two point
	my $dist = sqrt(
				($x1-$p1)**2 +
				($y1-$p2)**2 +
				($z1-$p3)**2
				); 
	return $dist;
}
sub StericImpedimentIntra{
	my ($syst)=@_;
	my @coords=@{$syst};
	my $flag=0;
	OUT: for (my $i = 0; $i < $natom; $i++) {
		for (my $j = $i+1; $j < $natom; $j++) {
			my $dist=distance_point($coords[1][$i],$coords[2][$i],$coords[3][$i],$coords[1][$j],$coords[2][$j],$coords[3][$j]);
			my $steric=verification($coords[0][$i],$coords[0][$j],$dist);
			if($steric==1){
				$flag=1;
				last OUT;
			}
		}
	}
	return $flag;
}
#################################################
sub PrintMutant{
	my ($syst, $tmp, $tmpline)=@_;
	my @coords=@{$syst};
	open (MUT, ">Mut_$name\_$tmp.com");
	print MUT "\%chk=Mut_$name\_$tmp.chk\n"; 	#name
	print MUT "\%NProc=$NProc\n";			#processors
	print MUT "\%mem=$Mem\n";				#ram
	print MUT "$tmpline\n\n";				#gaussian command
	print MUT "Mut_$name","_","$tmp\n\n";		#counter
	print MUT "$charge $multi","\n";		#charge and multiplicity
	for (my $i = 0; $i < $natom; $i++) {
		print MUT "$coords[0][$i]\t$coords[1][$i]\t$coords[2][$i]\t$coords[3][$i]\n";
	}
	print MUT "\n";
}
################################################# 
sub BuildMutant{
	my ($tmp, $command,@coordinates)=@_;
	# Calculate the probability to mutate
	#my $pcent=CalculateProbToMutatePosition();
	my $pcent=CalculateProbToMutateWithJ();
	#my $rand = (int rand(10000) +1)/10000;
	#print "Porcentajes: $rand y $pcent\n";
	#
	if($pcent>$FIXEDPCENTUMBRAL){
		#print "@coordinates\n";
		#var
		my $StericIsFalse=1;
		# only 30% of the atoms will suffer a mutation	
		my $atoms_to_change=int($natom*$PCENTATOMMUTATED);
		print "a cambiar: $atoms_to_change\n";
		my @new_coordinates=();
		while($StericIsFalse==1){
			my @name=();
			my @coords_x=();
			my @coords_y=();
			my @coords_z=();
			# the coordinates elements are shuffled
			my @recombination=fisher_yates_shuffle(\@coordinates);
			for (my $i = 0; $i < $natom; $i++) {
				my($element, $cx, $cy, $cz)=split (/\s+/,$recombination[$i]);
				push @name, $element;
				if($i < $atoms_to_change){
					my $radii_atom  = value_covalent_radii ($element);
					my $total_radii = ($radii_atom + $radii_atom);
					my $delta_radii = $delta + $total_radii ;
					#
					my $mult=int(rand(2));
					if ($mult==0){$mult=-1;}
					my $num = ($radii_atom + rand($delta_radii - $radii_atom))*$mult;
					#
					#print "dX $num\n";
					my $sum_x = $num + $cx ;
					print "$cx + dX $num = $sum_x\n";
					#
          			$mult=int(rand(2));
					if ($mult==0){$mult=-1;}
					$num         = ($radii_atom + rand($delta_radii - $radii_atom))*$mult;
					print "dY $num\n";
					my $sum_y = $num + $cy ;
					#
          			$mult=int(rand(2));
					if ($mult==0){$mult=-1;}
					$num         = ($radii_atom + rand($delta_radii - $radii_atom))*$mult;
					print "dZ $num\n\n";
					my $sum_z = $num + $cz ;		
					#
					#print "$element\t$sum_x\t$sum_y\t$sum_z\n";
					push @coords_x, $sum_x;
					push @coords_y, $sum_y;
					push @coords_z, $sum_z;
				}else{
					#print "$element\t$cx\t$cy\t$cz\n";
					push @coords_x, $cx;
					push @coords_y, $cy;
					push @coords_z, $cz;
				}
			}
			print "wea\n";
			@new_coordinates=([@name]
								,[@coords_x]
								,[@coords_y]
								,[@coords_z]);
			#print "@new_coordinates\n";
			$StericIsFalse= StericImpedimentIntra(\@new_coordinates);
		}
		PrintMutant(\@new_coordinates,$tmp, $command);
	}
}

sub GetByQuantity{
	#set variables.
	my($max,@lines)=@_;
	# $max is the maximum chosen
	my $size=length($max);
	$natom=$lines[0];
	my $aux=0;
	my $tmpline="";
	my @coords=();
	foreach my $x (@command) {
		$tmpline=$tmpline.$x."=";			
	}
	my $basin=0;
	shift(@lines);
	chop($tmpline);
	foreach $line(@lines){
		if($line==$natom){		#Al inicio de los tiempos, esto tenia un sentido, ahora no sirve.
			#print NEW $line;
			if($aux==1){
				my $magicLine=($natom+2+$basin)*($max-1);
				print "BEST $bestJ\n";
				print "WORST $lines[$magicLine]\n";
				$worstJ=(split("=",$lines[$magicLine]))[-1];
				#BuildMutant($tmp, $tmpline,@coords);
			}else{
				BuildMutant($tmp, $tmpline,@coords);
				@coords=();
			}
		}elsif ($line=~/=/){ 	#Only when the symbol "=" is read, then a new .Com file will be made.
			if($aux==$max){		#counter
				last;
			}
			$currentJ=(split("=", $line))[-1];
			if ($aux==0) { $bestJ=$currentJ;}
			$tmp=sprintf ("%0".$size."d",  $aux);	#number of 0 in the .com file name
			#$tmp=NumberCount($aux);
			$file=$name."_".$tmp;
			print "Printing $file\n";
			print COM "\n";
			open(COM, ">$file.com");
			#
			print COM "\%chk=$name\_$tmp.chk\n"; 	#name
			print COM "\%NProc=$NProc\n";			#processors
			print COM "\%mem=$Mem\n";				#ram
			print COM "$tmpline\n\n";				#gaussian command
			print COM "$name","_","$tmp\n\n";		#counter
			print COM "$charge $multi","\n";		#charge and multiplicity
			$aux++;
		}else{
			my $type=(split(" ",$line))[0];
			if($type ne "X"){
				print COM "$line";			#coordinates
				push @coords, $line;
			}elsif($aux==1){
				$basin++;
			}
		}
	}
	print COM "\n";						#last \n of thte last .com file (NECESSARY)
}

sub GetByEnergy{
	my($max,@lines)=@_;
	my $natom=$lines[0];

	my $flag=1;
	my $aux=0;

	foreach $line(@lines){
		if($flag==1){
			if($line==$natom){
				#print NEW $line;
			}elsif ($line=~/=/){
				chomp($line);
				$line=(split("Kcal\/mol",$line))[1];
				@full=(split(" ", $line));
				if($full[0] eq "="){
					$line=$full[1];
				}else{
					$line=$full[0];
				}
				$line=~s/[^-\d.]//g;
				#print "$line\n";
				
				if($line<$max){
					$tmp=NumberCount($aux);
					$file=$name."_".$tmp;
					print COM "\n";
					open(COM, ">$file.com");
					#
					print COM "\%chk=$name\_$tmp.chk\n";
					print COM "\%NProc=$NProc\n";
					print COM "\%mem=$Mem\n";
					print COM "$command\n\n";
					print COM "$name","_","$tmp\n\n";
					print COM "$charge $multi","\n";
					#
					$flag=1;
					$aux++;
				}else{
					$flag=0;
					last;
				}			
			}else{
				print COM "$line";
			}
		}
	}				#not used
}	
sub NumberCount{					#not used
	my($actual)=@_;
	my $back;
	if(length($actual)!=4){
		$dif=4-length($actual);
		$back="0"x$dif;
	}
	return ($back.$actual);
}
sub GetParams{
	#Read config file, set params
	open(CONFIG,"$configFile");
	my @lines=<CONFIG>;
	close(CONFIG);
	my $aux=0;
	foreach $line(@lines){
		chomp($line);
		if($line=~/ram/i){
			$Mem=trim(((split('=',$line))[-1])."GB");
			$aux++;
		}elsif($line=~/nproc/i){
			$NProc=trim((split('=',$line))[-1]);
			$aux++;
		}elsif($line=~/charge/i){
			$charge=trim((split('=',$line))[-1]);
			$aux++;
		}elsif($line=~/multiplicity/i){
			$multi=trim((split('=',$line))[-1]);
			$aux++;
		}elsif($line=~/command/i){
			@command=split("=",$line);
			shift(@command);
			#$command=trim((split("command",$line))[-1]);
			#$command=reverse($command);
			#chop($command);
			#$command=reverse($command);
			$aux++;
		}
	}
	if($aux<5){
		print "Not enought parameters in $configFile file\n";
		exit(1);
	}else{
		print "mem -$Mem-\nproc $NProc\ncm $charge $multi\ntheory @command\n";
	}
}




unlink glob "*.com"; #deletes all the .com files in route.
open (XYZ, "<$file");	
my @lines=<XYZ>;
close (XYZ);



if($type eq "-e"){
	print "Energy cut selected\n";
	GetParams();
	GetByEnergy($ARGV[1],@lines);
}elsif($type eq "-q"){
	print "Quantity cut selected\n";
	GetParams();
	$totalQuantity=$ARGV[1];
	GetByQuantity($ARGV[1],@lines);	
}elsif($type eq "-h" or $type eq "-help"){
	print "Use:\nEnergy cut:\n\t$0 -e [ENERGY] [PARAMETER FILE] [XYZ FILE] [OUTPUT_NAME]\n";
	print "\nBy Quantity:\n\t$0 -q [QUANTITY] [PARAMETER FILE] [XYZ FILE] [OUTPUT_NAME]\n";
	print "\n\n\nIMPORTANT:\n";
	print "Energy cut works with Kcal/mol only, and this parameters HAVE TO be in each structure of the xyz file\n\nEXAMPLE:\n";
	print "\nSi5B2 ev = 3 Kcal/mol = 4.3 CPUtime.....\nSi ...\nB ...\n";
}else{
	print "To display help:\n $0 -h\n"
}

