#!/usr/bin/env perl
#!/usr/bin/perl -s
my $output;
my %Atomic_number = ( '89'  => 'Ac', '13'  => 'Al', '95'  => 'Am', '51'  => 'Sb',	
	                  '18'  => 'Ar', '33'  => 'As', '85'  => 'At', '16'  => 'S',  
					  '56'  => 'Ba', '4'   => 'Be', '97'  => 'Bk', '83'  => 'Bi',	
                      '107' => 'Bh', '5'   => 'B', 	'35'  => 'Br', '48'  => 'Cd',	
	                  '20'  => 'Ca', '98'  => 'Cf',	'6'   => 'C',  '58'  => 'Ce',	
	                  '55'  => 'Cs', '17'  => 'Cl',	'27'  => 'Co', '29'  => 'Cu',	
	                  '24'  => 'Cr', '96'  => 'Cm', '110' => 'Ds', '66'  => 'Dy',	
	                  '105' => 'Db', '99'  => 'Es', '68'  => 'Er', '21'  => 'Sc',	
	                  '50'  => 'Sn', '38'  => 'Sr', '63'  => 'Eu', '100' => 'Fm',	
	                  '9'   => 'F',  '15'  => 'P',  '87'  => 'Fr', '64'  => 'Gd',	
	                  '31'  => 'Ga', '32'  => 'Ge', '72'  => 'Hf', '108' => 'Hs',	
                      '2'   => 'He', '1'   => 'H',  '26'  => 'Fe', '67'  => 'Ho',	
					  '49'  => 'In', '53'  => 'I',  '77'  => 'Ir', '70'  => 'Yb',
					  '39'  => 'Y',  '36'  => 'Kr', '57'  => 'La', '103' => 'Lr',	
					  '3'   => 'Li', '71'  => 'Lu', '12'  => 'Mg', '25'  => 'Mn',	
                      '109' => 'Mt', '101' => 'Md', '80'  => 'Hg', '42'  => 'Mo',	
					  '60'  => 'Nd', '10'  => 'Ne', '93'  => 'Np', '41'  => 'Nb',	
					  '28'  => 'Ni', '7'   => 'N',  '102' => 'No', '79'  => 'Au',	
					  '76'  => 'Os', '8'   => 'O', 	'46'  => 'Pd', '47'  => 'Ag',	
					  '78'  => 'Pt', '82'  => 'Pb',	'94'  => 'Pu', '84'  => 'Po',	
					  '19'  => 'K',  '59'  => 'Pr', '61'  => 'Pm', '91'  => 'Pa',	
					  '88'  => 'Ra', '86'  => 'Rn', '75'  => 'Re', '45'  => 'Rh',	
					  '37'  => 'Rb', '44'  => 'Ru', '104' => 'Rf', '62'  => 'Sm',
					  '106' => 'Sg', '34'  => 'Se', '14'  => 'Si', '11'  => 'Na',
					  '81'  => 'Tl', '73'  => 'Ta', '43'  => 'Tc', '52'  => 'Te',	
					  '65'  => 'Tb', '22'  => 'Ti', '90'  => 'Th', '69'  => 'Tm',	
					  '112' => 'Uub','116' => 'Uuh','111' => 'Uuu','118' => 'Uuo',	
					  '115' => 'Uup','114' => 'Uuq','117' => 'Uus','113' => 'Uut',
					  '92'  => 'U',  '23'  => 'V',  '74'  => 'W',  '54'  => 'Xe',
                      '30'  => 'Zn', '40'  => 'Zr', default=>'X');


sub convert_bohr_armstrong{my $s=shift; my $multi=$s*0.529177249; return $multi}


sub TAFFreader{
	#Reads files and prints basins in the .frag file
	my %index;
 	my $count_line=0,my $basins_end_position;
 	my @degenerated_ubications=();
 	my @degenerated_basin=();
 	my @lines=();
	if(open(TAFF,shift(@_))){
 		@lines=<TAFF>;
 	close(TAFF);
 	}else{
 		print "Error at open file\n";
 		exit(1);
 	}
 	foreach my $line(@lines){
 		#Save basin and degenerated atractor line positions
 		if($line=~/TOTAL/){
 			$basins_end_position=$count_line-2;
 		}
 		if($line=~/degenerate attractor/){
 			chomp($line);
 			chop($line);
 			push @degenerated_basin, (split(" ",$line))[-1];
 			push @degenerated_ubications, $count_line;
 		}	
 		$count_line++;
 	}
 	#turn it to an hash directory
 	@index{@degenerated_basin} = (0..$#degenerated_basin);
 	open(NEWTAFF,">$output.frag");
 	foreach my $basin(@lines[18..$basins_end_position]){
 		#start at 18 beacuase is the 1st line with basin information in Taff file
 		my @information=split(" ",$basin);
 		if(	grep (/^$information[0]$/,@degenerated_basin)){ #If the basin is a degenerated one it splits in the number of them
 			my $index = $index{$information[0]};
 			my @back=TAFFConverter($information[2],$degenerated_ubications[$index],@lines);
 			for (my $j = 0; $j < $back[-1]; $j++) {
 				print NEWTAFF "X\t$back[$j][1]\t$back[$j][2]\t$back[$j][3]\t$back[$j][0]\n";
 				
 			}
 			#last;
 		}else{
 			print NEWTAFF "X\t$information[3]\t$information[4]\t$information[5]\t$information[2]\n";
 			}
 	}
 	close(NEWTAFF);
 }

sub TAFFConverter{
 	my ($integral,$start,@lines)=@_;
 	my $aux=0;
 	my @back=();
 	my @return=();
	foreach my $line(@lines[($start+1)..$#lines]){
		chomp($line);
		my @info=split(" ",$line);
		if ($#info!=6) {
			last;
		}else{
			$aux++;
			push @back, [@info];
		}
	}
	my $newIntegral=$integral/$aux;
	for (my $i = 0; $i < $aux; $i++) {
		my  @data=($newIntegral,$back[$i][2],$back[$i][3],$back[$i][4]);
		push @return, [@data];
	}
	return (@return,$aux);
 }
sub CubeReader{
 	my $cubefile=shift(@_);
 	if(open(CUBE,"$cubefile")){
		my($natom)=0;
		my $aux=0;
		my $flag=0;
		open(NEWTAFF,">>$output.frag");
		foreach my $line(<CUBE>){
			if($aux==2){
				$natom=(split(" ",$line))[0];
				$num_atoms_global=$natom;
			}
			if($aux==6 || $flag==1){
				$flag=1;
				my @tmp=split(" ",$line);
				print NEWTAFF "$Atomic_number{$tmp[0]}\t",convert_bohr_armstrong($tmp[2]),"\t",convert_bohr_armstrong($tmp[3]),"\t",convert_bohr_armstrong($tmp[4]),"\n";
			}
			if($aux>=(5+$natom)){
				close(CUBE);
				last;
			}
			$aux++;
		}
		close(NEWTAFF);
	}else{
		print "Error at reading .cube file\n";
		exit(1);	
	}
	return @array_set_atoms;
 }
sub TAFF_Mode{
	my ($taff,$cube)=@_;
	($output_tmp = $taff) =~ s/\.[^.]+$//;
	$output = (split("/",$output_tmp))[-1];
	$output = (split("-integral",$output))[0];
	unlink "$output.tf";
	my @array_set_basin=TAFFreader($taff);
 	my @array_set_atoms=CubeReader($cube);	
}
sub PrintHelp{
	print "Welcome to TAFF file format converter.\n";
	print "This script will built a .tf format from the .cube and .txt outputs from TAFF.\n";
	print "\n\tUsage: $ perl $0 [TXT file with basin information] [cube file]\n";
	print "\n\tOutput: .tf file with the same name of the .cube file. All data is in Angstrom\n";
}
sub UseofScript{
	if($ARGV[0] eq "-h"){
		PrintHelp();
		exit(1);
	}
	if($#ARGV != 1){
		PrintHelp();
		exit(1);
	}else{
		#print "Building .tf format\n";
	}
}
UseofScript();
TAFF_Mode($ARGV[0],$ARGV[1]);
