#!/usr/bin/env perl
#!/usr/bin/perl -s

# Este programa debe hacer:
# - ler un archivo de congiguracion
# leer el nombre de archivos varios para extraer la informacion del: Atomos, numero y carga
# Leer globalemtne una carpeta para guardarlos en una lista
# Crear carpetas del repositorio siguiendo el orden:
# Atmos->AtomoNumero->App->Numero->f+f-log.
# Lanzar el TAFF apra cada fch en carpeta, guardar el log para cada uno.
# Mover los archivos en sus carpetas correspondientes.
# CAlcular el tiempo total del programa

#Datos de Vesion.
# Lee archivo de configuracion
# saca automaticamente el cation y anion en base a diferencias finitas
# llama al taff apra diferencais finitas.
# Crea las carpetas del repositorio.
# crea la ultima carpeta del repositior en favor al numero de datos ne memtoria.
# Lee ubicacoin del repositiors.
# Koopmans listo
# Tiempo

# Que no hará el programa:
# Verificar que lo calculado ya exista en el repositorio, es responsabilidad del usuario.

my $Compute;
my $Approx;
my $Approx_CuteName;
my $repositoryroute;
my $ConfifFile=$ARGV[0];
my $Taff_Maker="Frag_file_maker.pl";

 use Data::Dumper;
 use Benchmark;

sub ConfigFileReader{
	open(CONFIG,"$ConfifFile");
	my @lines=<CONFIG>;
	close(CONFIG);

	my $aux=0;

	foreach $line(@lines){
		chomp($line);
		my ($key, $value)=split("=",$line);
		if($key=~/compute/i){
			$Compute=$value;
			$aux++;
		}
		if($key=~/approximation/i){
			$Approx=$value;
			$aux++;
		}
		if($key=~/reppath/i){
			$repositoryroute=$value;
			$aux++;
		}
		
	}
	if($aux!=3){
		print "Incorrect number of arguments in $ConfifFile\n";
	}else{
		print "Correct number of arguments. This will take some time\n";
	}
}
sub GetFCHFiles{
	my @fch_files=();
	if($Approx=~/koopmans/i){
		@fch_files=glob "*.fch";		
	}elsif($Approx=~/finitedifferences/i || $Approx=~/finitediff/i){
		my @fch_files_cat=();
		my @fch_files_an=();
		my @fch_files_ne_1=(), @fch_files_ne_2=(), @fch_files_ne_3=(), @fch_files_ne_f=();
		@fch_files_cat=glob "*cat.fch";
		@fch_files_an=glob "*an.fch";
		#make the last array
		foreach $key(@fch_files_cat){
			my $tmp=(split("_cat",$key))[0];
			#print "->	$tmp\n";
			push @fch_files_ne_1,"$tmp.fch";
		}	 
		foreach $key(@fch_files_an){
			my $tmp=(split("_an",$key))[0];
			#print "->	$tmp\n";
			push @fch_files_ne_2,"$tmp.fch";
		}
		my @fch_files_ne_3=(@fch_files_ne_1,@fch_files_ne_2);
		#my @fch_files_ne_f = do { my %seen; grep { !$seen{$_}++ } @fch_files_ne_3 };
		my @fch_files_ne_f = do { my %seen; grep { !$seen{$_}++ } @fch_files_ne_3 };
		#make array
		push @fch_files,@fch_files_ne_f;
		#push (@fch_files,\@fch_files_cat);
		#push @fch_files,\@fch_files_an;
		
	}
	#print Dumper(@fch_files),"\n";
	return @fch_files;
}
sub ComputeType{
	my $back="";
	if($Compute=~/basin/i){
		$back="-f";
	}elsif($Compute=~/rho/i){
		$back="-e";
	}elsif($Compute=~/all/i){
		$back="-all";
	}else{
		print "Wrong compute value, please verify $ConfifFile\n";
		exit(1);
	}
	return $back;
}
sub verify_files{
	my ($base_path)=@_;
	#print "Evaluando el arcivo: $base_path\n";
	if(-f $base_path){
	#	print "$base_path is a plain file!\n";
		return 0;
	}else{
	#	print "bad\n";
		return 1;
	}

}
sub MakeRepository{
	my ($key, $type)=@_;
	$key=~ /([0-9]+)/; 
	my $number =$1;
	my ($atom)=(split/[0-9]/,$key)[0];
	#print "Atom: $atom\tNum: $number\n";
	#Go to repository
	#my $directory_name = "$repositoryroute/"|| '.';
	#opendir my $dh, $directory_name or die "Can't opendir '$directory_name': $!";
	#my $file_count = scalar grep { -d "$directory_name/$_" && !/^\.{1,2}$/ } readdir $dh;
	#closedir $dh or die "Can't closedir: $!";
	# Create repository like Atom/AtomNUmber/Aproximation.
	my $createdir_Atom = "mkdir -p $repositoryroute/$atom/$atom$number/$Approx_CuteName";
	#if(-d "$repositoryroute/$atom"){
	#	print "El este ya exite :p\n";
	#}else{
	system($createdir_Atom);
	#}
	####
	my $directory_name = "$repositoryroute/$atom/$atom$number/$Approx_CuteName/"|| '.';
	opendir my $dh, $directory_name or die "Can't opendir '$directory_name': $!";
	my $file_count = scalar grep { -d "$directory_name/$_" && !/^\.{1,2}$/ } readdir $dh;
	closedir $dh or die "Can't closedir: $!";
	#print "Carpetas Actualmente aca: $file_count\n";
	## This create the number of the carpet in specific.
	my $createdir_Atom_full = "mkdir -p $repositoryroute/$atom/$atom$number/$Approx_CuteName/$file_count";
	#if(-d "$repositoryroute/$atom/$atom$number/$Approx_CuteName/$file_count"){
	#	print "El este ya exite :p\n";
	#}else{
		system($createdir_Atom_full);
	#}

	system("mv f+$type-$key.frag $repositoryroute/$atom/$atom$number/$Approx_CuteName/$file_count/.");
	system("mv f-$type-$key.frag $repositoryroute/$atom/$atom$number/$Approx_CuteName/$file_count/.");
	system("mv $key.log $repositoryroute/$atom/$atom$number/$Approx_CuteName/$file_count/.");

}
sub CallTfMaker{
	my ($file, $type)=@_;
	#print ("perl $Taff_Maker $Approx_CuteName\_txt/f+_$type-$file-integral.txt $Approx_CuteName\_cube/f+_$type-$file.cube");
	system("perl $Taff_Maker $Approx_CuteName\_txt/f+$type-$file-integral.txt $Approx_CuteName\_cube/f+$type-$file.cube");
	system("perl $Taff_Maker $Approx_CuteName\_txt/f-$type-$file-integral.txt $Approx_CuteName\_cube/f-$type-$file.cube");

}
sub CallTAFF{
	#my ($array)=@_;
	my @fch_files=@_;
	my $cmp=ComputeType();

	if($Approx=~/koopmans/i){
		$Approx_CuteName="Koopmans";
		for (my $i = 0; $i <= $#fch_files; $i++) {
			print "Working on $fch_files[$i]\n\n";
			if(verify_files($fch_files[$i])==0){
				my $tmp_1=(split(".fch",$fch_files[$i]))[0];;
				#print ("TAFF -k $cmp $fch_files[$i] > $tmp_1.log\n");
				my $taff=system("TAFF -k $cmp $fch_files[$i] > $tmp_1.log");
				#print "BEFORE: $tmp_1\n";
				CallTfMaker($tmp_1,"koop");
				MakeRepository($tmp_1, "koop");
				
			}else{
				print "File \"$fch_files[$i]\" doesn't exist. Ignored\n";	
			}
		}
	}elsif($Approx=~/finitedifferences/i || $Approx=~/finitediff/i){
		$Approx_CuteName="FiniteDifferences";
		#ver tipo de compute
		for (my $i = 0; $i <= $#fch_files; $i++) {
		#verificar archivos; ver el posible 0 en cation_anion
		# 0 good, 1 bad;
		#also it calls taff
		print "Working on $fch_files[$i]\n\n";
		if(verify_files($fch_files[$i])==0){
			my $tmp_1=(split(".fch",$fch_files[$i]))[0];;
			#print "tmp es: $tmp_1\n";
			if(verify_files("$tmp_1\_cat.fch")==0 && verify_files("$tmp_1\_an.fch")==0){
				#print "Full\n";
				#print ("TAFF -fd $cmp $fch_files[$i] $tmp_1\_cat.fch $tmp_1\_an.fch\n");
				my $taff=system("TAFF -fd $cmp $fch_files[$i] $tmp_1\_cat.fch $tmp_1\_an.fch > $tmp_1.log");
			}
			elsif(verify_files("$tmp_1\_cat.fch")==0){
				#print "1 1 0\n";
				#print("TAFF -fd $cmp $fch_files[$i] $tmp_1\_cat.fch 0\n");
				my $taff=system("TAFF -fd $cmp $fch_files[$i] $tmp_1\_cat.fch 0");
			}elsif(verify_files("$tmp_1\_an.fch")==0){
				#print "1 0 1\n";
				#print ("TAFF -fd $cmp $fch_files[$i] 0 $tmp_1\_an.fch\n");
				my $taff=system("TAFF -fd $cmp $fch_files[$i] 0 $tmp_1\_an.fch");
			}else{
				#print "File \"$fch_files[$i]\" cation or anion file doesn't exist. Ignored\n";	
			}
			# dif es la palabra magiac  para los archivos de FiniteDifferences
			CallTfMaker($tmp_1,"_dif");
			MakeRepository($tmp_1, "_dif");
		}else{
			print "File \"$fch_files[$i]\" doesn't exist. Ignored\n";
		}
		}
		
	}
	
}

###########################################
if($#ARGV<0){
	print "Use:\nperl $0 ConfigFile.txt\n\n";
	exit(1);
}
#Time reader
my $tiempo_inicial = new Benchmark; #funcion para el tiempo de ejecucion del programa
$datestringStart = localtime();
#

#Read Config file
ConfigFileReader();

#Get files in Carpet
my @files=GetFCHFiles();
#Call TAFF and make repository
CallTAFF( @files);


my $tiempo_final = new Benchmark;
$datestringEnd = localtime();
#my $tiempo_secondBloq = timediff($tiempo_final, $tiempo_medio);
my $tiempo_allBloq = timediff($tiempo_final, $tiempo_inicial);
#print "\n\tTiempo de Ensamble: ",timestr($tiempo_firstBloq),"\n";
#print "\n\tTiempo de Creacion de Archivos: ",timestr($tiempo_secondBloq),"\n";
print "\n\tTiempo de Total: ",timestr($tiempo_allBloq),"\n";
print "\n";
