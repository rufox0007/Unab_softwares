#!/usr/bin/env perl
#!/usr/bin/perl -s

use Data::Dumper;

my $num_atoms_global;
my $num_basin_global;

sub TaffFileReader{
	my ($taffFile)=@_;
	#open tf file
	open(TAFF,$taffFile);
	my @information=<TAFF>;
	close(TAFF);
	#variable creations
	my $aux_na=0,$aux_ba=0;
	my @array_integral=();
 	my @array_X=();
 	my @array_Y=();
 	my @array_Z=();
 	my @array_atoms=();
	my @array_atoms_x=();
	my @array_atoms_y=();
	my @array_atoms_z=();
	#set data in varbiales
	foreach $line (@information){
		chomp($line);
		my @data=split(" ",$line);
		if($data[0] eq "X"){
			# X   CX  CY  CZ  INT
			push @array_integral, $data[4];
	 		push @array_X, $data[1];
	 		push @array_Y, $data[2];
	 		push @array_Z, $data[3];
	 		$aux_ba++;

		}else{
			push @array_atoms, $data[0];
			push @array_atoms_x, $data[1];
			push @array_atoms_y, $data[2];
			push @array_atoms_z, $data[3];
			$aux_na++;
		}
	}
	#counter of atoms and basins
	$num_atoms_global=$aux_na;
	$num_basin_global=$aux_ba;
	my @array_set_atoms  = ([@array_atoms],
                           [@array_atoms_x],
                           [@array_atoms_y],
                           [@array_atoms_z]);
	my @array_set_basin  = ([null],
                        [@array_integral],
                        [null],
                        [@array_X],
                        [@array_Y], 
                        [@array_Z],
                        [null],
                        [null]);
	my @matrix = ([@array_set_atoms],
              [@array_set_basin]);
	return @matrix;	#return variable
	
}

sub basin_integral_multi{
	my ($m1, $m2,$basin1,$basin2)=@_;
	my @matrix1=@{$m1};
	my @matrix2=@{$m2};
	my @array_multi=();
	for($i=0; $i<$basin1; $i++){
		for($j=0; $j<$basin2; $j++){
			my $tmp=$matrix1[1][1][$i]*$matrix2[1][1][$j];
			push(@array_multi,$tmp);
		}
	}
	return @array_multi;
}
# Distance between one point to all other in a coord xyz
sub distance_point {
	# array coords basin 1 and basin 2
	my ($p1,$p2,$p3, $x1, $y1, $z1) = @_;
	# measure distance between two point
	#print ("($x1-$p1)**2 +
	#			($y1-$p2)**2 +
	#			($z1-$p3)**2
	#			");
	my $dist = sqrt(
				($x1-$p1)**2 +
				($y1-$p2)**2 +
				($z1-$p3)**2
				); 
	return $dist;
}
sub distance_medition{
	my ($ba1, $ba2, $basin1, $basin2)=@_;
	my @big_array1=@{$ba1};
	my @big_array2=@{$ba2};
	my @dist_total=();
	#print "AHORA_ $basin1 y $basin2\n";
	for (my $i = 0; $i < $basin1; $i++) {
		for (my $j = 0; $j < $basin2; $j++) {
			my $tmp_dist=distance_point($big_array1[1][3][$i], $big_array1[1][4][$i],$big_array1[1][5][$i], $big_array2[1][3][$j], $big_array2[1][4][$j],$big_array2[1][5][$j] );
			push (@dist_total, $tmp_dist);
		}
	}
	#for (my $i = 0; $i < scalar (@{$big_array1[2]}); $i++) {
	#	for (my $j = 0; $j < scalar (@{$big_array2[2]}); $j++) {	
	#		my $tmp_dist = distance_point ( $big_array1[1][$i],$big_array1[2][$i],$big_array1[3][$i], $big_array2[1][$j],$big_array2[2][$j],$big_array2[3][$j]);
	#		push (@dist_total,$tmp_dist);
	#	}
	#}
	return @dist_total;
}
sub equation{
	my ($multi, $dist)=@_;
	my @b_multi=@{$multi};
	my @b_dist=@{$dist};
	my $sum=0;
	for (my $i = 0; $i < scalar (@b_multi); $i++) {
			#print "f+ * f- = $b_multi[$i] con distancia de $b_dist[$i]\t\n";
			my $division = ( $b_multi[$i] / $b_dist[$i] );
			#print "f+ * f- = $b_multi[$i] con distancia de $b_dist[$i]\t Division: $division\n";
			$sum+=$division;
		}
	return $sum;
}

print "Argumentos son: $ARGV[0] y $ARGV[1]\n";
my @fPlus_coordinates=TaffFileReader($ARGV[0]);
#print Dumper(@fPlus_coordinates),"\n";
#print "Atom: $num_atoms_global\tBasin: $num_basin_global\n";
my $basin1=$num_basin_global;
my @fMinus_coordinates=TaffFileReader($ARGV[1]);
my $basin2=$num_basin_global;
my @multi_integral=basin_integral_multi(\@fPlus_coordinates,\@fMinus_coordinates,$basin1,$basin2);
#print Dumper(@multi_integral),"\n\n";

my @distance_basins=distance_medition(\@fPlus_coordinates, \@fMinus_coordinates, $basin1, $basin2);
#print Dumper(@distance_basins),"t\n";

my $sum=equation(\@multi_integral,\@distance_basins);
print "Integral Coulombica: $sum\n";