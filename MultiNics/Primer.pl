#!/usr/bin/env perl
#!/usr/bin/perl -s

#############################
#############################3
##
#USE
#
use strict;
use warnings;
use Data::Dump qw(dump ddx);
use File::Basename;
use Tie::File;
###############################33
### BOX DATA
my $minX=0;
my $minY=0;
my $minZ=0;

my $maxX=0;
my $maxY=0;
my $maxZ=0;


my $NumPtsSide;
my $NumPtsX;
my $NumPtsY;
my $NumPtsZ;
my $delX;
my $delY;
my $delZ;

my $Box_x;
my $Box_y;
my $Box_z;	


my $origenX;
my $origenY;
my $origenZ;

my $deltaGlobal;
############
my %atom_counting = (default=>0);

############
my $MaxPtsPer;
#Config file data is turn in variables.
my $configFile;
my $NProc=4;
my $Mem="4GB";
my @command=();
my $ChargeMulti;
my $charge;
my $multi;
my $comName;

#############
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

sub ValidateFile{
	my ($fullspec)=@_;	
	die ("Can't open $fullspec\n") if !(-e $fullspec);
	my($file, $dir, $ext) = fileparse($fullspec, qr/\.[^.]*/);
	return ($file, $ext);
}
sub ReadXYZFile{
	my ($file, $tmpOption)=@_;
	my %coords;
	my $aux=0;
	open(FILE, "$file");
	my @lines=<FILE>;
	close(FILE);
	shift(@lines); #primera linea tiene el numero de atomos.
	shift(@lines); #segunda linea tiene el titulo
	#demas lineas tienen las coordedanas.
	foreach my $line(@lines){
		my ($a, $x, $y, $z)=split(" ", $line);
		#This option works only for 1 recombination, more than 1 will duplicate the amount of maximmum atoms in memory
		if ($tmpOption==1) {$atom_counting{$a}++;$atom_counting{"all"}++;}
		$coords{$aux}=[$a,$x,$y,$z];
		$aux++;
	#	push @at, $a;
	#	push @cx, $x;
	#	push @cy, $y;
	#	push @cz, $z;
	}
	#dump(%atom_counting);
	return %coords;
}
sub CreateMesh{
	my ($hashCoordAtoms)=@_;
	my %coordinates=%{$hashCoordAtoms};
	
	#print "Griilla va entre:\n";
	#print "X = $minX - $maxX\n";
	#print "Y = $minY - $maxY\n";
	#print "Z = $minZ - $maxZ\n";
	unlink "tmpGrill.xyz";
	open(GRILLA, ">tmpGrill.xyz");
	#exit(1);
	#$#='%.3f';
	#for (my $i = $minX; $i <= $maxX; $i=$i+$delX) {
	#	for (my $j = $minY; $j <= $maxY; $j=$j+$delY) {
#		for (my $h = $minZ; $h <= $maxZ; $h=$h+$delZ) {
			#	$i=float("%.3f"),$i;
	for (my $i = 0; $i < $NumPtsX; $i++) {
		for (my $j = 0; $j < $NumPtsY; $j++) {
			for (my $h = 0; $h < $NumPtsZ; $h++) {
				my $cx=($i*$delX)+$minX;
				my $cy=($j*$delY)+$minY;
				my $cz=($h*$delZ)+$minZ;
				print GRILLA "Bq\t";
				printf GRILLA ("%.3f\t",$cx);
				printf GRILLA ("%.3f\t",$cy);
				printf GRILLA ("%.3f\n",$cz);
			}
		}
	}
	foreach my $dot (sort (keys %coordinates)){
		printf GRILLA "${$coordinates{$dot}}[0]\t";
		printf GRILLA ("%.3f\t",${$coordinates{$dot}}[1]);
		printf GRILLA ("%.3f\t",${$coordinates{$dot}}[2]);
		printf GRILLA ("%.3f\n",${$coordinates{$dot}}[3]);
		#${$coordinates{$dot}}[1]\t${$coordinates{$dot}}[2]\t${$coordinates{$dot}}[3]\n";		
	}
	close(GRILLA);
}
sub SetMeshVariables{
	#my ($pts,$dx,$dy,$dz)=@_;
	if($Box_x<0 || $Box_y<0 || $Box_z<0 ){
		print "Box can't have negative values.\n";
		exit(1);
	}
	print "CAJA: $Box_x\t$Box_y\t$Box_z\n";
	$delX= $Box_x/($NumPtsSide-1);
	$delY= $Box_y/($NumPtsSide-1);
	$delZ= $Box_z/($NumPtsSide-1);
	#$NumPtsX=(($Box_x)/$deltaGlobal)+1;
	#$NumPtsY=(($Box_y)/$deltaGlobal)+1;
	#$NumPtsZ=(($Box_z)/$deltaGlobal)+1;

	#print "Puntos seran: X $NumPtsX\n";
	#print "Puntos seran: Y $NumPtsY\n";
	#print "Puntos seran: Z $NumPtsZ\n";
	$NumPtsX=$NumPtsY=$NumPtsZ=$NumPtsSide;
	if($Box_x==0){$NumPtsX=1;}
	if($Box_y==0){$NumPtsY=1;}
	if($Box_z==0){$NumPtsZ=1;}
	print "Puntos seran: X $NumPtsX\n";
	print "Puntos seran: Y $NumPtsY\n";
	print "Puntos seran: Z $NumPtsZ\n";
	
	my $sum= $NumPtsX*$NumPtsY*$NumPtsZ;
	$atom_counting{"Ghosts"}=$sum;
	
	print "Tota Points:\t$sum\tPoints x Side:\t$NumPtsSide\n";
	print "Spacing X:\t$delX\n";
	print "Spacing Y:\t$delY\n";
	print "Spacing Z:\t$delZ\n";

	
	$maxX=($Box_x/2)+$origenX;
	$maxY=($Box_y/2)+$origenY;
	$maxZ=($Box_z/2)+$origenZ;

	$minX=$maxX-$Box_x;
	$minY=$maxY-$Box_y;
	$minZ=$maxZ-$Box_z;
	######
	#dump (%atom_counting);																																																													
	print "\nBox:\n";
	print "X = $minX - $maxX\n";
	print "Y = $minY - $maxY\n";
	print "Z = $minZ - $maxZ\n\n";

}
##################################################################
############### COM File creators
sub SetNumberPointPerCOMS{
	my ($hashCoordAtoms)=@_;
	my %coordinates=%{$hashCoordAtoms};
	
	my $totalAtoms=$atom_counting{"all"}+$atom_counting{"Ghosts"};
	my $totalNumberOfComs=$atom_counting{"Ghosts"}/$MaxPtsPer;
	my $extra=0;
	
	#if($totalNumberOfComs=~/./){
	if($totalNumberOfComs - int($totalNumberOfComs)>0){
		$extra=$atom_counting{"Ghosts"}-(int($totalNumberOfComs)*$MaxPtsPer);
	}
	$totalNumberOfComs=int($totalNumberOfComs);
	print "Total a crear: $totalNumberOfComs (.com) de $MaxPtsPer Pts + 1 archivo .com con $extra Pts\n";				
	#open(TMP,"tmpGrill.xyz");
	#my @pts=<TMP>;
	tie my @pts, 'Tie::File', "tmpGrill.xyz";
	#close(TMP);
	my $aux=0;
	my $tmpline="";
	#linea gaussian test
	foreach my $x (@command) {
		$tmpline=$tmpline.$x."=";			
	}
	chop($tmpline);
	#exit(1);
	#####
	for (my $i = 0; $i < (($totalNumberOfComs*$MaxPtsPer)+$extra); $i+=$MaxPtsPer) {
		my $tmp=sprintf ("%0".length($totalNumberOfComs)."d",  $aux);
		#printf ">$comName\_$tmp.com\n";
		open(COM,">$comName\_$tmp.com");
		print COM "\%NProc=$NProc\n";			#processors
		print COM "\%mem=$Mem\n";				#ram
		print COM "$tmpline\n\n";				#gaussian command
		print COM "$comName\_$tmp title\n\n";		#counter
		print COM "$charge $multi","\n";		#charge and multiplicity	
		for (my $j = $i; $j < $i+$MaxPtsPer && $j<(scalar(@pts)-$atom_counting{"all"}); $j++) {
			print COM "$pts[$j]\n";
		#	print "j= $j y da  $pts[$j]"
		}
		foreach my $dot (sort (keys %coordinates)){
			#print COM "${$coordinates{$dot}}[0]\t${$coordinates{$dot}}[1]\t${$coordinates{$dot}}[2]\t${$coordinates{$dot}}[3]\n";		
			printf COM "${$coordinates{$dot}}[0]\t";
			printf COM ("%.3f\t",${$coordinates{$dot}}[1]);
			printf COM ("%.3f\t",${$coordinates{$dot}}[2]);
			printf COM ("%.3f\n",${$coordinates{$dot}}[3]);
		
		}
		print COM "\n";
		close(COM);
		$aux++;
		#last;
	}
}
##################################################################
sub GetParams{
	#Read config file, set params
	open(CONFIG,"$configFile");
	my @lines=<CONFIG>;
	close(CONFIG);
	my $aux=0;
	foreach my $line(@lines){
		my $letter = substr($line, 0, 1);
		if($letter ne "#"){
			chomp($line);
			if($line=~/ram/i){
				$Mem=trim(((split('=',$line))[-1])."GB");
				#$aux++;
			}elsif($line=~/nproc/i){
				$NProc=trim((split('=',$line))[-1]);
				#$aux++;
			}elsif($line=~/charge/i){
				$charge=trim((split('=',$line))[-1]);
				$aux++;
			}elsif($line=~/multiplicity/i){
				$multi=trim((split('=',$line))[-1]);
				$aux++;
			}elsif($line=~/command/i){
				@command=split("=",$line);
				shift(@command);
				$aux++;
			}
		}
	}
	if($aux<3){
		print "Not enought parameters in $configFile file\n";
		exit(1);
	}
}
##################################################################
print "\n\tMinimum Viable Product.... Nombre del programa\n";

#my @files= glob "*.xyz";

if($#ARGV<10){
	print "Recuerda:\n";
	print "$0 input.xyz Box_X Box_Y Box_Z origin_X origin_Y Origin_Z NumPtsXLado NumPtsComs(99) Miconf.txt OutputName\n\n";
	exit(1);
}

my $file=$ARGV[0];
$configFile=$ARGV[9];
GetParams();
$comName= $ARGV[10];
#sleep archivo XYZ. salida es un array  @a=([@atom],[@x],[@y],[@z])
ValidateFile($file);
my %structure = ReadXYZFile($file,1);
#dump %structure;
#Centrar molecula
##############
#$NumPtsX=$ARGV[0];
#$NumPtsY=$ARGV[1];	
#$NumPtsZ=$ARGV[2];
#$delX=$ARGV[3];
#$delY=$ARGV[4];
#$delZ=$ARGV[5];
$Box_x=$ARGV[1];
$Box_y=$ARGV[2];
$Box_z=$ARGV[3];
$origenX=$ARGV[4];
$origenY=$ARGV[5];
$origenZ=$ARGV[6];
$NumPtsSide= $ARGV[7];
$MaxPtsPer = $ARGV[8];
unlink "*.com";
SetMeshVariables();
#exit(1);
CreateMesh(\%structure);
SetNumberPointPerCOMS(\%structure);
##############
print "Salida en archivo tmpGrill.xyz\n";