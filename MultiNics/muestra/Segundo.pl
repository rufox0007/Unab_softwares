#!/usr/bin/env perl
#!/usr/bin/perl -s

use strict;
use warnings; no warnings 'uninitialized';
use Data::Dump qw(dump ddx);
use File::Basename;
use Tie::File;
my $MeshFile;
my $comName;
my $outFormat;
###
my $linesCorrectlyTurned=0;
my $totalAtoms=0;
my $flagStart=0;
my $flagEnd=1;

sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };
sub ReadOutsSetInfo{
	#print "1\n";
	my ($file)=@_;
	
	my $lineNumber=0;
	my $atomStarts;
	my $atomEND=-1;
	my $tensorStarts;
	my $tensorEND;
	open(COM, $file);
	#Set line numbers
	while (my $line=<COM>) {
		if($line=~/Center/ && $line=~/Coordinates/ && $line=~/Angstroms/){
			$atomStarts=$lineNumber+3;
		}
		if($line=~/Distance/ && $line=~/matrix/ && $line=~/angstroms/){
			$atomEND=$lineNumber-1;
		}elsif($line=~/Rotational/ && $line=~/GHZ/ && $atomEND==-1){
			$atomEND=$lineNumber-1;
		}
		if($line=~/SCF/ && $line=~/GIAO/ && $line=~/shielding/ && $line=~/tensor/){
			$tensorStarts=$lineNumber+1;
		}
		if($line=~/Population/ && $line=~/SCF/ && $line=~/density/ && $line=~/analysis/){
			$tensorEND=$lineNumber;
		}
		$lineNumber++;
	}
	close(COM);
	tie my @data, 'Tie::File', $file;
	#print "$atomStarts $atomEND $tensorStarts $tensorEND\n";
	#print "$data[$atomStarts]\n";
	#print "$data[$atomEND]\n";
	#print "$data[$tensorStarts]\n";
	#print "$data[$tensorEND]\n";

	my %coordsComs;
	# Count atoms and points
	for (my $i = $atomStarts; $i < $atomEND; $i++) {
		my @coords=split(" ",$data[$i]);
	#	print "$coords[1]\n";
		if($coords[1]!=0 && $flagStart==0){ #es un atomo
			$totalAtoms++;
		}
		$coordsComs{$i-$atomStarts}=[@coords];
	}
	#print "atomos: $totalAtoms\n";
	my $totalData;
	#special case with the last com, we print system info
	if($flagEnd==0){
		$totalData=($atomEND-$atomStarts);
	}else{
		$totalData=($atomEND-$atomStarts)-$totalAtoms;
	}
	$flagStart=1;
	####
	my %tensor;
	my $aux=0;
	#Get tensor information
	for (my $i = $tensorStarts; $i < $tensorStarts+($totalData*9); $i+=9) {
		chomp($data[$i]);
				my $Iso=(split(" ",$data[$i]))[4];
				my $Ani=(split(" ",$data[$i]))[7];
				my $XX=(split(" ",$data[$i+1]))[1];
				my $YY=(split(" ",$data[$i+2]))[3];
				my $ZZ=(split(" ",$data[$i+3]))[5];
				my $vec1=trim($data[$i+6]);
				my $vec2=trim($data[$i+7]);
				my $vec3=trim($data[$i+8]);
		$tensor{$aux}=[($Iso,$Ani,$XX,$YY,$ZZ,$vec1,$vec2,$vec3)];
		#dump %tensor;
		$aux++;
	}
	####
	#
	tie my @meshdata, 'Tie::File', $MeshFile;
	# Turn old xyz point data file in new .dat file with
	# type Coords, iso ani xx yy zz vectors1 2 3
	#File with X Y Z 
	# se rellena z primero, leugo pasa a Y , finalmente cambia X
	open(NEWMESH, ">>TMP.dat");
	for (my $i = $linesCorrectlyTurned; $i < $linesCorrectlyTurned+$totalData && $i<=scalar(@meshdata); $i++) {
		my @meshCoords=split(" ",$meshdata[$i]);
		print ".";
		#print "$meshCoords[1]  ${$coordsComs{$i-$linesCorrectlyTurned}}[3]\n";
		#print "$meshCoords[2]  ${$coordsComs{$i-$linesCorrectlyTurned}}[4]\n";
		#print "$meshCoords[3]  ${$coordsComs{$i-$linesCorrectlyTurned}}[5]\n";
		if($meshCoords[1]==${$coordsComs{$i-$linesCorrectlyTurned}}[3] && $meshCoords[2]==${$coordsComs{$i-$linesCorrectlyTurned}}[4] && $meshCoords[3]==${$coordsComs{$i-$linesCorrectlyTurned}}[5]){
			#print "Pico $i\n";
			print NEWMESH "$meshCoords[0]\t$meshCoords[1]\t$meshCoords[2]\t$meshCoords[3]\t";
			#my $newXX= ${$tensor{$i-$linesCorrectlyTurned}}[2]*(-1);
			#my $newYY= ${$tensor{$i-$linesCorrectlyTurned}}[3]*(-1);
			#my $newZZ= ${$tensor{$i-$linesCorrectlyTurned}}[4]*(-1);
			#my $inPlane=($newXX+$newYY)/3;
			#my $outPlane=$newZZ/3;
			print NEWMESH "${$tensor{$i-$linesCorrectlyTurned}}[0]\t";
			print NEWMESH "${$tensor{$i-$linesCorrectlyTurned}}[1]\t";
			print NEWMESH "${$tensor{$i-$linesCorrectlyTurned}}[2]\t";
			print NEWMESH "${$tensor{$i-$linesCorrectlyTurned}}[3]\t";
			print NEWMESH "${$tensor{$i-$linesCorrectlyTurned}}[4]\t";
			print NEWMESH "${$tensor{$i-$linesCorrectlyTurned}}[5]\t";
			print NEWMESH "${$tensor{$i-$linesCorrectlyTurned}}[6]\t";
			print NEWMESH "${$tensor{$i-$linesCorrectlyTurned}}[7]\n";

		}else{
			print "ERROR\n";
		}
	}
	close(NEWMESH);
	$linesCorrectlyTurned+=$totalData;

}
sub ReadMeshCOMS{
	my @coms=glob "$comName*.$outFormat";
	my $last=pop @coms;
#	dump @coms;
	unlink "TMP.dat";
	foreach my $com (@coms){
		ReadOutsSetInfo($com);
	#	last;
	}
	$flagEnd=0;
	ReadOutsSetInfo($last);

}
print "Usage:\n$0 tmpGrill.xyz comName [Formtat]\nformat = log para .log; out para .out\n";
$MeshFile=$ARGV[0];
$comName= $ARGV[1];
$outFormat= $ARGV[2];
ReadMeshCOMS();
print "\n";