#!/usr/bin/env perl
#!/usr/bin/perl -s

use strict;
use warnings; no warnings 'uninitialized';
use Data::Dump qw(dump ddx);
use File::Basename;
use Tie::File;

my $datFile;
############
my $atomTotal;
my $Xl=-1;
my $Yl=-1;
my $Zl=-1;
my $delta;
my $del_x;
my $del_y;
my $del_z;
my $NumPts;
my $NX;
my $NY;
my $NZ;
my $origen_x;
my $origen_y;
my $origen_z;
my $out;

sub ReadDatGetInfo{
	my($same1, $same2, $ori1, $ori2)=@_;
	print "($same1, $same2, $ori1, $ori2)\n";
	tie my @meshdata, 'Tie::File', $datFile;
	my %magicalLines;
	my $aux=0;
	foreach my $data (@meshdata){
		my @information=split(" ",$data);
		#print "$information[$same1]\n";
		if($information[$same1]==$ori1 && $information[$same2]==$ori2){
			#print "$information[0]\t$information[1]\t$information[2]\t$information[3]\n";
			$magicalLines{$aux}=[@information];
			$aux++;
		}elsif($information[0] ne "Bq"){
			$magicalLines{$aux}=[@information];
			$aux++;
		}
	}
	return %magicalLines;
}
sub ReadDatGetPlane{
	my ($choosen, $ori1)=@_;
	tie my @meshdata, 'Tie::File', $datFile;
	my %magicalLines;
	my $aux=0;	
	foreach my $data (@meshdata){
		my @information=split(" ",$data);
		#print "$information[$same1]\n";
		if($information[$choosen]==$ori1){
			#print "$information[0]\t$information[1]\t$information[2]\t$information[3]\n";
			$magicalLines{$aux}=[@information];
			$aux++;
		}elsif($information[0] ne "Bq"){
			$magicalLines{$aux}=[@information];
			$aux++;
		}
	}
	return %magicalLines;
}
sub FIPC{
	my($origin,$eje, $type)=@_;
	#print "eje: $eje\n";
	my @origen=split(" ",$origin);
	my %magicalLines;
	my $outputShow;
	my $PrintC;
	my $PrintCC;
	if ($eje==1) {
		# $i, 0, 0
		%magicalLines=ReadDatGetInfo(2,3,$origen[1], $origen[2]);
		$outputShow=1;
		$PrintC="X";
		$PrintCC="XX";
	} elsif ($eje==2) {
		# $0 $i 0
		%magicalLines=ReadDatGetInfo(1,3,$origen[0], $origen[2]);
		$outputShow=2;
		$PrintC="Y";
		$PrintCC="YY";
	} elsif ($eje==3) {
		# 0 0 $i41
		%magicalLines=ReadDatGetInfo(1,2,$origen[0], $origen[1]);
		$outputShow=3;
		$PrintC="Z";
		$PrintCC="ZZ";
	} else{
		print "Axis not supported\n";
	}
	my $total=keys %magicalLines;	
	if($total!=0){
		#dump %magicalLines;
		#print "FIPC!!!\n";
		print "Output file name:";
		my $FIPCoutput=<STDIN>;
		open(FIPC, ">$FIPCoutput");
		if($type==1){
			print FIPC "Type\t$PrintC\tXX\tYY\tZZ\tIn-Plane\tOut-Plane\n\n";
		}elsif($type==2){
			print FIPC "Type\t$PrintC\t$PrintCC\tIso\tAni\n\n";
		}else{
			print FIPC "Type\t$PrintC\tXX\tYY\tZZ\tIso\tIn-Plane\tOut-Plane\tAni\n\n";
		}
		
		foreach my $id (0..$total){
			#print "$id\n";
			my $newXX=${$magicalLines{$id}}[6]*(-1);
			my $newYY=${$magicalLines{$id}}[7]*(-1);
			my $newZZ=${$magicalLines{$id}}[8]*(-1);
			my $inplane;
			my $outplane;
			if($eje==1){
				$inplane=($newZZ+$newYY)/3;
				$outplane=$newXX/3;
			}elsif($eje==2){
				$inplane=($newXX+$newZZ)/3;
				$outplane=$newYY/3;
			}else{
				$inplane=($newXX+$newYY)/3;
				$outplane=$newZZ/3;
			}
			print FIPC "${$magicalLines{$id}}[0]\t";
			print FIPC "${$magicalLines{$id}}[$outputShow]\t";
			#printf COM ("%.3f\t",${$coordinates{$dot}}[1]);
			if($type==1){
				printf FIPC ("%.3f\t",$newXX);
				printf FIPC ("%.3f\t",$newYY);
				printf FIPC ("%.3f\t",$newZZ);
				printf FIPC ("%.3f\t",$inplane);
				printf FIPC ("%.3f\n",$outplane);
			}elsif($type==2){
				printf FIPC ("%.3f\t",${$magicalLines{$id}}[$outputShow+5]*(-1));
				printf FIPC ("%.3f\t",${$magicalLines{$id}}[4]);
				printf FIPC ("%.3f\n",${$magicalLines{$id}}[5]);
			}else{
				printf FIPC ("%.3f\t",$newXX);
				printf FIPC ("%.3f\t",$newYY);
				printf FIPC ("%.3f\t",$newZZ);
				printf FIPC ("%.3f\t",${$magicalLines{$id}}[4]);
				printf FIPC ("%.3f\t",$inplane);
				printf FIPC ("%.3f\t",$outplane);
				printf FIPC ("%.3f\n",${$magicalLines{$id}}[5]);
			}
		}
		close(FIPC);
	}
}
sub Menu{
	print "Programa MultiNics\n\n";
	while (1) {
		print "1\tCalculate FIPC\n";
		print "2\tCalculate SCAN\n";
		print "3\tCalculate FIPC & SCAN\n";
		print "4\tCalculate Plane Data\n";
		print "5\tCalculate Plane Vectors\n";
		print "6\tExit\n";
		my $option=<STDIN>;
		if ($option==1) {
			#make FIPC
			print "FIPC calculation chosen. (-1 to exit)\n";
			print "Origin Coordinates:";
			my $nicOption=<STDIN>;
			if($nicOption ne "-1\n"){
				#llamar optcion del nic
				print "Axis (1 X; 2 Y; 3 Z):";
				my $axis=<STDIN>;
				FIPC($nicOption,$axis,1);
			}
		} elsif ($option==2) {
			print "SCANS calculation chosen. (-1 to exit)\n";
			print "Origin Coordinates:";
			my $nicOption=<STDIN>;
			if($nicOption ne "-1\n"){
				#llamar optcion del nic
				print "Axis (1 X; 2 Y; 3 Z):";
				my $axis=<STDIN>;
				FIPC($nicOption,$axis,2);
			}
		} elsif ($option==3) {
			print "Both calculation chosen. (-1 to exit)\n";
			print "Origin Coordinates:";
			my $nicOption=<STDIN>;
			if($nicOption ne "-1\n"){
				#llamar optcion del nic
				print "Axis (1 X; 2 Y; 3 Z):";
				my $axis=<STDIN>;
				FIPC($nicOption,$axis,3);
			}
		}elsif($option==4){
			print "Select a plane (-1 to exit)\n";
			print "Plane (1 XY; 2 YZ; 3 XZ: ";
			my $plane=<STDIN>;
			if($plane ne "-1 \n"){
				print "Number of Angstrom:";
				my $distancePlane=<STDIN>;
				Plane($plane,$distancePlane);	
			}
		}elsif($option==5){
			print "Select a plane (-1 to exit)\n";
			print "Plane (1 XY; 2 YZ; 3 XZ: ";
			my $plane=<STDIN>;
			if($plane ne "-1 \n"){
				print "Number of Angstrom:";
				my $distancePlane=<STDIN>;
				VectorPlaneVTI($plane,$distancePlane);	
			}				
		}else{
			exit(1);
		}
	}
}
sub GetParamOFDat{
	tie my @meshdata, 'Tie::File', $datFile;
	my @first=split(" ",$meshdata[0]);
	my @second=split(" ",$meshdata[1]);
	
	$origen_x= $first[1];
	$origen_y= $first[2];
	$origen_z= $first[3];
	for (my $i = 1; $Xl==-1; $i++) {
		my @tmp=split(" ",$meshdata[$i]);
		if($origen_z==$tmp[3] && $Zl==-1){
			$Zl=($i);
			$Yl=-2;
		}elsif($Yl==-2 && $origen_y==$tmp[2]){
			$Yl=($i)/$Zl;
		}elsif($tmp[0] ne "Bq"){
			$Xl=$i;
			last;
		}
	}
	$atomTotal=(scalar @meshdata)-$Xl;
	$NumPts=$Xl;
	$Xl=$Xl/($Yl*$Zl);
	$delta=abs($first[3]-$second[3]);
	my @last=split(" ",$meshdata[$NumPts-1]);
	print "Atoms\t$atomTotal\n";
	print "Number of Points\t$NumPts\n";
	print "Delta\t$delta\n\n";

	#$NX=abs($last[1]-$first[1]);
	#$NY=abs($last[2]-$first[2]);
	#$NZ=abs($last[3]-$first[3]);
	
}
sub Plane{
	my ($plane, $distance)=@_;
	my %magicalLines;
	if ($plane==1) {
		# $i, 0, 0
		%magicalLines=ReadDatGetPlane(3,$distance);
	} elsif ($plane==2) {
		# $0 $i 0
		%magicalLines=ReadDatGetPlane(1,$distance);
	} elsif ($plane==3) {
		# 0 0 $i41
		%magicalLines=ReadDatGetPlane(2,$distance);
	} else{
		print "Axis not supported\n";
	}
	
	my $total=(keys %magicalLines)-$atomTotal;
	my %hashSort;
	if($total!=0){
		foreach my $id (0..($total-1)){
			$hashSort{$id}=[${$magicalLines{$id}}[1],${$magicalLines{$id}}[2],${$magicalLines{$id}}[3],${$magicalLines{$id}}[0],${$magicalLines{$id}}[8],${$magicalLines{$id}}[18],${$magicalLines{$id}}[19],${$magicalLines{$id}}[20]];
		}
		
	}
	my @KeysSorted= sort { $hashSort{$a}[2]<=>$hashSort{$b}[2] || $hashSort{$a}[1]<=>$hashSort{$b}[1] || $hashSort{$a}[0]<=>$hashSort{$b}[0]} keys %hashSort;
	my @xtotal;
	my @ytotal;
	my @ztotal;
	foreach my $data(@KeysSorted){
		push @xtotal, $hashSort{$data}[0];
		push @ytotal, $hashSort{$data}[1];
		push @ztotal, $hashSort{$data}[2];
	}
	#Start vtk data
	$origen_x=$hashSort{$KeysSorted[0]}[0];
	$origen_y=$hashSort{$KeysSorted[0]}[1];
	$origen_z=$hashSort{$KeysSorted[0]}[2];
	
	############
	my %seen;
	my @uniqX= grep {! $seen{$_}++} @xtotal;
	undef %seen;
	my @uniqY= grep {! $seen{$_}++} @ytotal;
	undef %seen;
	my @uniqZ= grep {! $seen{$_}++} @ztotal;
	#print "HOPE\n";
	$NX=scalar @uniqX;
	$NY=scalar @uniqY;
	$NZ=scalar @uniqZ;
	#print "\n";
	VTKStructuredDataHeader();
	VTKStructuredBody(\%hashSort,\@KeysSorted);
	#VTKVectorsDataHeader(\%hashSort,\@KeysSorted);
}
sub VectorPlaneVTI{
my ($plane, $distance)=@_;
	my %magicalLines;
	print "yay\n";
	if ($plane==1) {
		# $i, 0, 0
		%magicalLines=ReadDatGetPlane(3,$distance);
	} elsif ($plane==2) {
		# $0 $i 0
		%magicalLines=ReadDatGetPlane(1,$distance);
	} elsif ($plane==3) {
		# 0 0 $i41
		%magicalLines=ReadDatGetPlane(2,$distance);
	} else{
		print "Axis not supported\n";
	}
	
	my $total=(keys %magicalLines)-$atomTotal;
	my %hashSort;
	if($total!=0){
		print "Data to graphic (0 Iso; 1 Ani; 2 XX; 3 YY; 4 ZZ): ";
		my $pos=<STDIN>;
		foreach my $id (0..($total-1)){
			$hashSort{$id}=[${$magicalLines{$id}}[1],${$magicalLines{$id}}[2],${$magicalLines{$id}}[3],${$magicalLines{$id}}[0],${$magicalLines{$id}}[$pos+4],${$magicalLines{$id}}[18],${$magicalLines{$id}}[19],${$magicalLines{$id}}[20]];
		}
		
	}
	my @KeysSorted= sort { $hashSort{$a}[2]<=>$hashSort{$b}[2] || $hashSort{$a}[1]<=>$hashSort{$b}[1] || $hashSort{$a}[0]<=>$hashSort{$b}[0]} keys %hashSort;
	my @xtotal;
	my @ytotal;
	my @ztotal;
	foreach my $data(@KeysSorted){
		push @xtotal, $hashSort{$data}[0];
		push @ytotal, $hashSort{$data}[1];
		push @ztotal, $hashSort{$data}[2];
	}
	#Start vtk data
	$origen_x=$hashSort{$KeysSorted[0]}[0];
	$origen_y=$hashSort{$KeysSorted[0]}[1];
	$origen_z=$hashSort{$KeysSorted[0]}[2];
	
	############
	my %seen;
	my @uniqX= grep {! $seen{$_}++} @xtotal;
	undef %seen;
	my @uniqY= grep {! $seen{$_}++} @ytotal;
	undef %seen;
	my @uniqZ= grep {! $seen{$_}++} @ztotal;
	#print "HOPE\n";
	$NX=scalar @uniqX;
	$NY=scalar @uniqY;
	$NZ=scalar @uniqZ;

	$del_x=abs($uniqX[0]-$uniqX[1]);
	$del_y=abs($uniqY[0]-$uniqY[1]);
	$del_z=abs($uniqZ[0]-$uniqZ[1]);

	VTKVectorsDataHeader(\%hashSort,\@KeysSorted);

}
sub VTKVectorsDataHeader{
	print "Output file name: ";
	$out=<STDIN>;
	open(VTI, ">$out");
	my ($data,$keys)=@_;
	my %hash=%{$data};
	my @llaves=@{$keys};
	print VTI "<?xml version=\"1.0\"?>\n";
	print VTI "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
	print VTI "\t<ImageData WholeExtent=\"";
	print VTI " 0 ".($NX-1);
	print VTI " 0 ".($NY-1);
	print VTI " 0 ".($NZ-1)."\"";
	print VTI " Origin=\"";
	print VTI "$origen_x $origen_y $origen_z\"";
	print VTI " Spacing=\"";
	print VTI "$del_x $del_y $del_z\">\n";	
	print VTI "\t<Piece Extent=\"";
	print VTI " 0 ".($NX-1);
	print VTI " 0 ".($NY-1);
	print VTI " 0 ".($NZ-1)."\">\n";
	print VTI "\t<PointData Scalars=\"scalars\">\n";
	print VTI "\t<DataArray Name=\"vectors\" type=\"Float64\" NumberOfComponents=\"3\" Format=\"ascii\">\n";
	my $aux=0;
	my $aux2=0;
	my $towrite="";
	#open(VTK,">>$out");
	for (my $var = 0; $var < keys %hash; $var++) {
		#print "wea";
		my $point=($hash{$llaves[$var]}[4]);
#		print "$point\n";
		my $vx=($hash{$llaves[$var]}[5])*$point;
		my $vy=($hash{$llaves[$var]}[6])*$point;
		my $vz=($hash{$llaves[$var]}[7])*$point;
#		print "\t\t$vx\t$vy\t$vz\n";
		#print "$point\n";
		#$aux++;
		#$aux2++;
		print VTI "\t\t$vx\t$vy\t$vz\n";
		#$towrite=$towrite."$vx $vy $vz";
		#if($aux2<($NX*$NY)){
		#	if($aux<$NX){
		#		$towrite=$towrite." ";
		#	}else{
		#		$towrite=$towrite." \n";
		#		print VTI "$towrite";
		#		$towrite="";
		#		$aux=0;
		#	}
		#}else{
		#	$towrite=$towrite." \n\n ";
		#	print VTI "$towrite";	
		#	$aux2=0;
		#	$aux=0;
		#	$towrite="";
		#}
#		last;
	}
	print VTI "\n\t</DataArray>\n\t</PointData>\n";
	print VTI "\t<CellData Scalars=\"foo\">\n";
	for (my $var = 0; $var < keys %hash; $var++) {
		my $point=($hash{$llaves[$var]}[4]);
		print VTI "\t\t$point\n";
	}
	print VTI "\t\t</CellData>\n\t\t</Piece>\n\t</ImageData>\n</VTKFile>\n";
	close(VTI);
}
sub VTKStructuredDataHeader{
	print "Output file name: ";
	$out=<STDIN>;
	open(VTK, ">$out");
	print VTK "# vtk DataFile Version 3.0\n";
	print VTK "VTK file with points\n";
	print VTK "ASCII\n";
	print VTK "DATASET STRUCTURED_POINTS\n";
	print VTK "DIMENSIONS $NX $NY $NZ\n";
	print VTK "ORIGIN $origen_x $origen_y $origen_z\n";
	print VTK "SPACING $delta $delta $delta\n";
	print VTK "POINT_DATA ",$NX*$NY*$NZ,"\n";
	print VTK "SCALARS scalars float 1\n";
	print VTK "LOOKUP_TABLE default\n";
	close(VTK);
}
sub VTKStructuredBody{
	my ($data,$keys)=@_;
	my %hash=%{$data};
	my @llaves=@{$keys};
	my $aux=0;
	my $aux2=0;
	my $towrite=" ";
	open(VTK,">>$out");
	for (my $var = 0; $var < keys %hash; $var++) {
		#print "wea";
		my $point=($hash{$llaves[$var]}[4])*(-1);
		#print "($point)\n";
		$aux++;
		$aux2++;
		$towrite=$towrite.$point;
		if($aux2<($NX*$NY)){
			if($aux<$NX){
				$towrite=$towrite." ";
			}else{
				$towrite=$towrite." \n";
				print VTK "$towrite";
				$towrite=" ";
				$aux=0;
			}
		}else{
			$towrite=$towrite." \n\n ";
			print VTK "$towrite";	
			$aux2=0;
			$aux=0;
			$towrite="";
		}
	}
	close(VTK);
}
print "$0 TMP.dat\n";
$datFile=$ARGV[0];
GetParamOFDat();
Menu();