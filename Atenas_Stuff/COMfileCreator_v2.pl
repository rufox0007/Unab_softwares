#!/usr/bin/perl -w
#!/usr/bin/env perl


#Version notes:
# com files correctly
# numeration of files done correctly
# commented



#perl script.pl MODE ARGS CONFIG INPUT NAME_OF_COM
my $file=$ARGV[3]; #XYZ file name
my $name=$ARGV[4]; #NAMe of the .com files
my $type=$ARGV[0];	#Option to work with. Q for quantity, e for energy cut.
my $configFile= $ARGV[2];	#Config file with all the data for .com file


#Config file data is turn in variables.
my $NProc=4;
my $Mem="4GB";
my @command=();
my $ChargeMulti;
my $charge;
my $multi;

################
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

sub GetByQuantity{
	#set variables.
	my($max,@lines)=@_;
	# $max is the maximum chosen
	my $size=length($max);
	my $natom=$lines[0];
	my $aux=0;
	my $tmpline="";
	foreach my $x (@command) {
		$tmpline=$tmpline.$x."=";			
	}
	open(COM,">a");
	chop($tmpline);
	foreach $line(@lines){
		if($line eq $natom){		#Al inicio de los tiempos, esto tenia un sentido, ahora no sirve.
			#print NEW $line;
		}elsif ($line=~/=/){ 	#Only when the symbol "=" is read, then a new .Com file will be made.
			if($aux==$max){		#counter
				last;
			}
			$tmp=sprintf ("%0".$size."d",  $aux);	#number of 0 in the .com file name
			#$tmp=NumberCount($aux);
			$file=$name."_".$tmp;
			#print "$file\n";
			print COM "\n";
			close(COM);
			open(COM, ">$file.com");
			#
			print COM "\%chk=$name\_$tmp.chk\n"; 	#name
			print COM "\%NProc=$NProc\n";			#processors
			print COM "\%mem=$Mem\n";				#ram
			print COM "$tmpline\n\n";				#gaussian command
			print COM "$ARGV[3]","_","$tmp\n\n";		#counter
			print COM "$charge $multi","\n";		#charge and multiplicity
			$aux++;
		}else{
			my $type=(split(" ",$line))[0];
			select(COM);
			if($type ne "X"){
				print COM "$line";			#coordinates
			}
		}
	}
	print COM "\n";						#last \n of thte last .com file (NECESSARY)
	close (COM);
	unlink "a";
}	
sub NumberCount{					#not used
	my($actual)=@_;
	my $back;
	if(length($actual)!=4){
		$dif=4-length($actual);
		$back="0"x$dif;
	}
	return ($back.$actual);
}
sub GetParams{
	#Read config file, set params
	open(CONFIG,"$configFile");
	my @lines=<CONFIG>;
	close(CONFIG);
	my $aux=0;
	foreach $line(@lines){
		my $letter = substr($line, 0, 1);
		if($letter ne "#"){
			chomp($line);
			if($line=~/ram/i){
				$Mem=trim(((split('=',$line))[-1])."GB");
				#$aux++;
			}elsif($line=~/nproc/i){
				$NProc=trim((split('=',$line))[-1]);
				#$aux++;
			}elsif($line=~/charge/i){
				$charge=trim((split('=',$line))[-1]);
				$aux++;
			}elsif($line=~/multiplicity/i){
				$multi=trim((split('=',$line))[-1]);
				$aux++;
			}elsif($line=~/command/i){
				@command=split("=",$line);
				shift(@command);
				#$command=trim((split("command",$line))[-1]);
				#$command=reverse($command);
				#chop($command);
				#$command=reverse($command);
				$aux++;
			}
		}
	}

	if($aux<3){
		print "Not enought parameters in $configFile file\n";
		exit(1);
	}else{
		#print "mem -$Mem-\nproc $NProc\ncm $charge $multi\ntheory @command\n";
	}
}
#unlink glob "*.com"; #deletes all the .com files in route.
open (XYZ, "<$file");	
my @lines=<XYZ>;
close (XYZ);

if($type eq "-e"){
	print "Energy cut selected\n";
	GetParams();;
}elsif($type eq "-q"){
	#print "Quantity cut selected\n";
	GetParams();
	GetByQuantity($ARGV[1],@lines);	
}elsif($type eq "-h" or $type eq "-help"){
	print "Use:\nEnergy cut:\n\t$0 -e [ENERGY] [PARAMETER FILE] [XYZ FILE] [OUTPUT_NAME]\n";
	print "\nBy Quantity:\n\t$0 -q [QUANTITY] [PARAMETER FILE] [XYZ FILE] [OUTPUT_NAME]\n";
	print "\n\n\nIMPORTANT:\n";
	print "Energy cut works with Kcal/mol only, and this parameters HAVE TO be in each structure of the xyz file\n\nEXAMPLE:\n";
	print "\nSi5B2 ev = 3 Kcal/mol = 4.3 CPUtime.....\nSi ...\nB ...\n";
}else{
	print "To display help:\n $0 -h\n"
}

