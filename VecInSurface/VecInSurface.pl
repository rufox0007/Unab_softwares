#!/usr/bin/perl

use strict;
use warnings; no warnings 'uninitialized';
#use bignum;
use Data::Dump qw(dump ddx);


#read vtk and vti
# ask for a isovalue
# choose vtk values with isovalue < given
# print only vti vector with the exact coordinates of choosen vtk ones.

#my $vtkFile = $ARGV[0];
my $vtiFile = $ARGV[0];
my $value = $ARGV[1];

#sub GetVTIInformation{
	#print "$vtiFile\n";
	open(FILE, "$vtiFile");
	my @lines =<FILE>;
	#dump @lines;
	close(FILE);
	my @size = split(" ",$lines[2]);
	my $fullgrid = (($size[3]+1) * ($size[5]+1) * ($size[7]+1)) + 10;
	#dump @size;
	#print "tamaño total seria de $fullgrid\n";
	my @scalar = @lines[ $fullgrid .. ($#lines - 4) ];
	my @vectors = @lines[ 6 .. ($fullgrid-5) ];
	open(NEW, ">new.vti");
	foreach my $i (0..5){
		print NEW $lines[$i];
	}
	print "$vectors[0]\n$vectors[-1]";
	my $j=0;
	foreach my $data (@scalar){
		#print "$data\n $scalar[-1]";
		if( $data >= $value){
			print NEW $vectors[$j];
		}else{
			print NEW " 0.0\t0.0\t0.0\n";
		}
		$j++;
		#exit(1);
	}
	print NEW "
    </DataArray>
    </PointData>
    <CellData Scalars=\"foo\">\n";
    print NEW @scalar;
    print NEW "    </CellData>
    </Piece>
    </ImageData>
 </VTKFile>\n";
 close(NEW);
