#!/usr/bin/perl

use strict;
use warnings; no warnings 'uninitialized';
#use bignum;
use Data::Dump qw(dump ddx);


#read vtk and vti
# ask for a isovalue
# choose vtk values with isovalue < given
# print only vti vector with the exact coordinates of choosen vtk ones.

#my $vtkFile = $ARGV[0];
my $vtiFileMod = $ARGV[0];
my $vtiFileVec = $ARGV[1];

#sub GetVTIInformation{
	#print "$vtiFile\n";
	open(FILE, "$vtiFileMod");
	my @lines =<FILE>;
	#dump @lines;
	close(FILE);
	open(VEC, "$vtiFileVec");
	my @linesvectores = <VEC>;
	close(VEC);
	my @size = split(" ",$linesvectores[2]);
	my $fullgrid = (($size[3]+1) * ($size[5]+1) * ($size[7]+1)) + 10;
	#dump @size;
	#print "tamaño total seria de $fullgrid\n";
	#my @scalar = @lines[ $fullgrid .. ($#lines - 4) ];
	my @scalar = @lines[ 6 .. ($#lines-5) ];
	my @vectors = @linesvectores[ 6 .. ($fullgrid-5) ];
	my @moduleVectors = @linesvectores[ $fullgrid .. ($#linesvectores - 4) ];
	print "$vectors[0] and $vectors[-1]\n";

	open(POS, ">PositivoMOD.vti");
	open(NEG, ">NegativoMOD.vti");

	open(VPOS, ">PositivoVEC.vti");
	open(VNEG, ">NegativoVEC.vti");
	
	foreach my $i (0..5){
		print POS $lines[$i];
		print NEG $lines[$i];
		print VPOS $linesvectores[$i];
		print VNEG $linesvectores[$i];
	}
	#print "$scalar[0]\n$scalar[-1]";
	my $j=0;
	foreach my $data (@scalar){
		#print "$data\n $scalar[-1]";
		my @tmp = split(" ", $data);
		
		foreach my $single (@tmp){
			if( $single >= 0){
				print POS "$single\t";
				print NEG "0.0\t";
				print VPOS "$vectors[$j]";
				print VNEG "0.0\t0.0\t0.0\n";
			}else{
				print POS "0.0\t";
				print NEG "$single\t";
				print VNEG "$vectors[$j]";
				print VPOS "0.0\t0.0\t0.0\n";
			}
			$j++;
		}
		print POS "\n";
		print NEG "\n";
		
		#exit(1);
	}
	print POS "    </DataArray>
    </PointData>
    </Piece>
    </ImageData>
 </VTKFile>
";
	print NEG "    </DataArray>
    </PointData>
    </Piece>
    </ImageData>
 </VTKFile>
";
	
 close(POS);
 close(NEG);
 #Vectores aqui
	print VPOS "
    </DataArray>
    </PointData>
    <CellData Scalars=\"foo\">\n";
    	print VNEG "
    </DataArray>
    </PointData>
    <CellData Scalars=\"foo\">\n";

    print VPOS @moduleVectors;
    print VNEG @moduleVectors;
    print VPOS "    </CellData>
    </Piece>
    </ImageData>
 </VTKFile>\n";
 print VNEG "    </CellData>
    </Piece>
    </ImageData>
 </VTKFile>\n";
 close(VPOS);
 close(VNEG);