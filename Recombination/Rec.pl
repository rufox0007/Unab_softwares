#!/usr/bin/env perl
#!/usr/bin/perl -s

use Math::Matrix;
 use Data::Dump qw(dump ddx);

use strict;
use warnings; no warnings 'uninitialized';

# Vector normal del plano.
my @v=();
# Hash contador de atomos x tipo
my %atom_counting = (default=>0);
# tmp
my $separation="";
#my $number;

sub ReadXYZFile{
	my ($file, $tmpOption)=@_;

	my @at = ();
	my @cx = ();
	my @cy = ();
	my @cz = ();

	open(FILE, "$file");
	my @lines=<FILE>;
	close(FILE);
	shift(@lines); #primera linea tiene el numero de atomos.
	shift(@lines); #segunda linea tiene el titulo
	#demas lineas tienen las coordedanas.
	foreach my $line(@lines){
		my ($a, $x, $y, $z)=split(" ", $line);
		#This option works only for 1 recombination, more than 1 will duplicate the amount of maximmum atoms in memory
		if ($tmpOption==1) {$atom_counting{$a}++;$atom_counting{"all"}++;}
		push @at, $a;
		push @cx, $x;
		push @cy, $y;
		push @cz, $z;
	}
	dump(%atom_counting);
	my @coords=([@at],
				[@cx],
				[@cy],
				[@cz]);
	return @coords;
}
sub CreateRandomPlane{
	#this is my idea...
	my $a1=(rand(3))-(rand(3));
	my $b1=(rand(3))-(rand(3));
	my $c1=(rand(3))-(rand(3));

	my $a2=(rand(3))-(rand(3));
	my $b2=(rand(3))-(rand(3));
	my $c2=(rand(3))-(rand(3));
	
	# vector en plano
	@v=($b1*$c2-$c1*$b2, 
		$c1*$a2-$a1*$c2,
		$a1*$b2-$b1*$a2,0);
	#Pts del plano
	$separation="X\t$a1  $b1 $c1\nX\t$a2  $b2 $c2\nX\t0\t0\t0\n";
	print $separation;
	print "Plano: $v[0]*X + $v[1]*Y + $v[2]*Z = 0\n";
}
sub PlanePosition{
	my (@coords)=@_;
	my @cx = @{$coords[1]}; 
	my @cy = @{$coords[2]}; 
	my @cz = @{$coords[3]}; 
	my @a = @{$coords[0]}; 
	my @dots=();
	my %dotHash;
	for (my $var = 0; $var <= $#cx; $var++) {
		my @v_coordinates=($cx[$var],$cy[$var],$cz[$var],1);
		my $dot=dotprod(\@v_coordinates, \@v);
		$dot=$dot.$var;				#scurity measure
		$dotHash{$dot}=[$a[$var],@v_coordinates];  # hash(Dot product)=Atom, x, y, z
		push @dots, $dot;			#array of dots products
	}
	#hash type DotProduct->Coordinates, Array of DotsProduct
	return (\%dotHash, \@dots);
}
sub CombineMolecule{
	#This function combine 2 molecules taking the dotproduct and coordinates.
	my ($hash1, $hash2, $sort1 , $sort2)=@_;
	#$hash is the hash variable (by reference) with dotProdut->coordinates; 
	#$sort is the dotproduct sorted in both sizes, molecule 1 will be from greater to least and molecule 2 will be from least to greater value	
	# atom_counting is a global variable, has the information of how many atoms from an specific type there are
	my $natom=$atom_counting{"all"};
	delete $atom_counting{"all"};
	my %verifyCorrectNumberOfAtoms;		# hash tmp for later use
	my @finalCoordinates=();
	# desreference arrays
	my @dotSortPlusMinus = @{$sort1};
	my @dotSortMinusPlus = @{$sort2};
	# desrereference hashs
	my %coordsByHash1=%{$hash1};
	my %coordsByHash2=%{$hash2};
	# get al the jeys from the global hash
	my @keys_atoms= keys %atom_counting;
	# constructor for the tmp hash, all values are set to 0
	#open(TMP,">>tmp$number.xy");
	foreach my $keyInMemory(@keys_atoms){
		$verifyCorrectNumberOfAtoms{$keyInMemory}=0;
	}
	my ($i, $minor, $big)=(0, 0, 0);	
	for (my $i = 0; $i <= $natom*2; $i++) {
		my @tmpCoord;
		# greater dot value from molecule 1
		if(($i%2)==0){
			@tmpCoord=$coordsByHash1{$dotSortPlusMinus[$big]};
			$big++;
		# lesser dot value from molecule 2
		}else{
			@tmpCoord=$coordsByHash2{$dotSortMinusPlus[$minor]};
			$minor++;
		}
		my $typeatom=$tmpCoord[0][0];
		# if the number of a type of atoms is lesser than the maximum value for the specific atom, then the coordinates are save to the final array
		if($verifyCorrectNumberOfAtoms{$typeatom} < $atom_counting{$typeatom}){
			$verifyCorrectNumberOfAtoms{$typeatom}++;
			push @finalCoordinates, @tmpCoord;
			#Print TMP file
			#print TMP ($#finalCoordinates+1)."\n$i TITLE\n".$separation;
			#for (my $var = 0; $var <= $#finalCoordinates; $var++) {
			#	print TMP "$finalCoordinates[$var][0]\t$finalCoordinates[$var][1]\t$finalCoordinates[$var][2]\t$finalCoordinates[$var][3]\n";
			#}
			if(($#finalCoordinates+1)==$natom){
				last;
			}
		}
	}
	#close(TMP);
	#Simple print to show coordinates
	for (my $var = 0; $var <= $#finalCoordinates; $var++) {
		print "$finalCoordinates[$var][0] $finalCoordinates[$var][1] $finalCoordinates[$var][2] $finalCoordinates[$var][3]\n";
	}
	delete @atom_counting{@keys_atoms};
	return @finalCoordinates;
}
sub SortDotMolecule{
	#Simple function to  sort an array
	my ($dot, $option)=@_;
	my @dots= @{$dot};
	my @sorted=();
	if($option==1){
		#mayor a menor
		@sorted= sort {$b <=> $a} @dots
	}else{
		#menor a mayor
		@sorted= sort {$a <=> $b} @dots
	}
	return @sorted;
}
sub dotprod{
	#Dot Product, takes 2 vector and returns dotProduct
	#Internet helps!!!
    my($vec_a, $vec_b) = @_;
    die "they must have the same size\n" unless @$vec_a == @$vec_b;
    my $sum = 0;
    $sum += $vec_a->[$_] * $vec_b->[$_] for 0..$#$vec_a;
    return $sum;
}
# compute the center of mass
sub measure_center {
	my ($coord_x,$coord_y,$coord_z) = @_;
	my $num_data = scalar (@{$coord_x});
	my @array  = ();
	my $weight = 1;
	# variable sum
	my $sum_weight = 0;
	my $sum_x = 0;
	my $sum_y = 0;
	my $sum_z = 0;
	for ( my $j = 0 ; $j < $num_data ; $j = $j + 1 ){
		$sum_weight+= $weight;
		$sum_x+= $weight * @$coord_x[$j];
		$sum_y+= $weight * @$coord_y[$j];
		$sum_z+= $weight * @$coord_z[$j];		
	}
	my $com_x = $sum_x / $sum_weight;
	my $com_y = $sum_y / $sum_weight;
	my $com_z = $sum_z / $sum_weight;
	# array
	@array = ($com_x,$com_y,$com_z);
	# return array	
	return @array;
}
###################################
# Returns the additive inverse of v(-v)
sub vecinvert {
	my ($center_mass) = @_;
	my @array         = ();
	foreach my $i (@$center_mass) {
		my $invert        = $i * -1;
		$array[++$#array] = $invert; 
	}	
	# return array	
	return @array;
}
###################################
# Returns the vector sum of all the terms.
sub vecadd {
	my ($coord_x,$coord_y,$coord_z,$vecinvert_cm ) = @_;
	my $num_data = scalar (@{$coord_x});
	my @array   = ();
	my $sum_coord_x;
	my $sum_coord_y;
	my $sum_coord_z;
	# array 
	my @array_x = ();
	my @array_y = ();
	my @array_z = ();
	for ( my $i = 0 ; $i < $num_data ; $i = $i + 1 ){	
		$sum_coord_x = @$coord_x[$i]+@$vecinvert_cm[0] ; 
		$sum_coord_y = @$coord_y[$i]+@$vecinvert_cm[1] ;
		$sum_coord_z = @$coord_z[$i]+@$vecinvert_cm[2] ;
		# save array
		$array_x[++$#array_x] = $sum_coord_x;
		$array_y[++$#array_y] = $sum_coord_y;
		$array_z[++$#array_z] = $sum_coord_z;
	}
	@array = ( [@array_x], 
              [@array_y], 
              [@array_z] ); 
	# return array	
	return @array;
}
sub CenterMolecule{
	my ($input)=@_;
	my @problematic_molecule=@{$input};
	my @coord_x=@{$problematic_molecule[1]};
	my @total_array = ();
	
	my @array_center_mass = measure_center(\@{$problematic_molecule[1]},\@{$problematic_molecule[2]},\@{$problematic_molecule[3]});
	my @array_vecinvert   = vecinvert(\@array_center_mass);
	my @array_catersian   = vecadd (\@{$problematic_molecule[1]},\@{$problematic_molecule[2]},\@{$problematic_molecule[3]},\@array_vecinvert);
	@total_array=(${problematic_molecule[0]},
					$array_catersian[0],
					$array_catersian[1],
					$array_catersian[2]);
	return (@total_array);	
}
###################################################
############MAIN
###################################################
#my $file1= $ARGV[0];
#my $file2= $ARGV[1];

my $numberOfRec=$ARGV[0];

my @files= glob "*.xyz";


for (my $h = 0; $h < $numberOfRec; $h++) {
	print "\n";
	my $pos1=int rand($#files+1);
	my $pos2=int rand($#files+1);
	while ($pos1==$pos2) {
		$pos2=int rand($#files+1);
	}
#	print "-> $pos1 - $pos2\n";
	my $file1=$files[$pos1];
	my $file2=$files[$pos2];

#Lee archivo XYZ. salida es un array  @a=([@atom],[@x],[@y],[@z])
my @frag1=ReadXYZFile($file1,1);
my @frag2=ReadXYZFile($file2,2);

my @centered=CenterMolecule(\@frag1);
my @centered2=CenterMolecule(\@frag2);

#Create random plane with a vector
CreateRandomPlane();

# get coordinates and DotProduct for each molecule for the plane
my ($hashWithCoordinates_1, $PP_coords1)=PlanePosition(@centered);
my ($hashWithCoordinates_2, $PP_coords2)=PlanePosition(@centered2);

#sort molecules, 1 is decreasing order; 2 is increasing order.
my @sort1=SortDotMolecule($PP_coords1, 1);
my @sort2=SortDotMolecule($PP_coords2, 2);
#use data to combine 2 molecules. returns array with coordinates
CombineMolecule($hashWithCoordinates_1, $hashWithCoordinates_2, \@sort1, \@sort2);
}